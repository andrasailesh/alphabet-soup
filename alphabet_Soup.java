import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class alphabet_Soup {

	public static void main(String[] args) throws FileNotFoundException {
		File file = new File("input");
		
		ArrayList<String> fileinput = new ArrayList<>();
		
		Scanner fileScan = new Scanner(file);
		while(fileScan.hasNextLine())
		{
			StringTokenizer str = new StringTokenizer(fileScan.nextLine());
			while(str.hasMoreTokens())
			{
				fileinput.add(str.nextToken());
			
			}
		}
		
		//System.out.println(fileinput);
		
		
		String mul = fileinput.get(0);
		String[] arrOfStr = mul.split("x", 0);
		
		int rowCount = (Integer.parseInt(arrOfStr[0]));
		
		int colCount = (Integer.parseInt(arrOfStr[1]));
		
		
		
		String[][] twoarray  = new String[rowCount][colCount];
		
		int total = rowCount*colCount;
		
		ArrayList<String> matrix  = new ArrayList<>();

		for(int x = 1; x< total+1;x++)
		{
			matrix.add(fileinput.get(x));
		}
		
		//System.out.println(matrix);
		
	int count =0;
	
	
	for(int x = 0; x < rowCount; x++)
	{
		for(int y = 0; y < colCount; y++)
		{
			twoarray[x][y] = matrix.get(count);
			count++;
		}
	}
	

		

	
	
	

	
	ArrayList<String> out = new ArrayList<>();
	
	for(int x = matrix.size()+1; x < fileinput.size();x++)
	{
		out.add(fileinput.get(x));
	}
	
	
	
	
	
	
	
	
	getList(out,rowCount,colCount,twoarray);
	
	
	}
	
	
	
		
	


	public static void getList(ArrayList<String> out,int rowC, int colC,String[][] twoarray) {
	    ArrayList<Integer> counter = new ArrayList<>();
	    ArrayList<String> List = out;
	    ArrayList<String> getword = new ArrayList<String>();
	    ArrayList<ArrayList<String>> outer = new ArrayList<ArrayList<String>>();
	    
	    for (int x = 0; x < List.size(); x++) {
	        counter.add(List.get(x).length()); // Add the length to the counter list
	        
	        for (int y = 0; y < List.get(x).length(); y++) {
	            char c = List.get(x).charAt(y);
	            String ne = Character.toString(c);
	            getword.add(ne);
	           
	            
	            if (getword.size() == counter.get(x)) {
	                outer.add(getword);
	                getword = new ArrayList<String>(); // Create a new ArrayList
	            }
	        }
	    }
	    
	 
	    
		getnum(outer,rowC,colC,twoarray,out);
		
	}
	
	
	
	public static void getnum(ArrayList<ArrayList<String>> out,int r, int c,String[][] twoarr,ArrayList<String> ou)
	{
		
		int rowCount = r;
		int colCount = c;
		String[][] twoArray = twoarr;
		
		
		for(int x =0; x< rowCount;x++)
		{
			for(int y =0;y<colCount;y++)
			{
				//System.out.print(twoArray[x][y]);
			}
			//System.out.println();
		}
		
		ArrayList<ArrayList<String>> outer = new ArrayList<ArrayList<String>>(out);
		
		ArrayList<String> nums = new ArrayList<>();
		
		for (int AoA = 0; AoA < outer.size(); AoA++) {
		    boolean sequenceFound = false;

		    for (int row = 0; row < rowCount && !sequenceFound; row++) {
		        for (int col = 0; col < colCount && !sequenceFound; col++) {
		            int inner;

		            // Check for horizontal match
		            for (inner = 0; inner < outer.get(AoA).size() && col + inner < colCount; inner++) {
		                if (!twoArray[row][col + inner].equals(outer.get(AoA).get(inner))) {
		                    break;
		                }
		            }
		            if (inner == outer.get(AoA).size()) {
		                for (int i = 0; i < inner; i++) {
		                    String numR = row + "";
		                    String numC = (col + i)+"";
		                    nums.add(numR);
		                    nums.add(numC);
		                    
		                }
		                sequenceFound = true;
		            }

		            // Check for vertical match
		            for (inner = 0; inner < outer.get(AoA).size() && row + inner < rowCount && !sequenceFound; inner++) {
		                if (!twoArray[row + inner][col].equals(outer.get(AoA).get(inner))) {
		                    break;
		                }
		            }
		            if (inner == outer.get(AoA).size()) {
		                for (int i = 0; i < inner; i++) {
		                    String numR = (row + i) + "";
		                    String numC = col + "";
		                    nums.add(numR);
		                    nums.add(numC);
		                }
		                sequenceFound = true;
		            }
		            
		            
		         // Check for top-right diagonal match
		            for (inner = 0; inner < outer.get(AoA).size() && row + inner < rowCount && col + inner < colCount && !sequenceFound; inner++) {
		                if (!twoArray[row + inner][col + inner].equals(outer.get(AoA).get(inner))) {
		                    break;
		                }
		            }
		            if (inner == outer.get(AoA).size()) {
		                for (int i = 0; i < inner; i++) {
		                   
		                    String numR =(row + i) + "";
		                    String numC = (col + i) + "";
		                    nums.add(numR);
		                    nums.add(numC);
		                }
		                sequenceFound = true;
		            }
		            
		         // Check for bottom-left diagonal match
		            for (inner = 0; inner < outer.get(AoA).size() && row + inner < rowCount && col - inner >= 0 && !sequenceFound; inner++) {
		                if (!twoArray[row + inner][col - inner].equals(outer.get(AoA).get(inner))) {
		                    break;
		                }
		            }
		            if (inner == outer.get(AoA).size()) {
		                for (int i = 0; i < inner; i++) {
		                 
		                    String numR = (row + i) + "";
		                    String numC = (col - i) + "";
		                    nums.add(numR);
		                    nums.add(numC);
		                }
		                sequenceFound = true;
		            }

		            // Check for reverse horizontal match
		            for (inner = 0; inner < outer.get(AoA).size() && col - inner >= 0 && !sequenceFound; inner++) {
		                if (!twoArray[row][col - inner].equals(outer.get(AoA).get(inner))) {
		                    break;
		                }
		            }
		            if (inner == outer.get(AoA).size()) {
		                for (int i = 0; i < inner; i++) {
		                    
		                    String numR =row+"";
		                    String numC = (col - i) + "";
		                    nums.add(numR);
		                    nums.add(numC);
		                }
		                sequenceFound = true;
		            }


		            // Check for reverse right diagonal match
		            for (inner = 0; inner < outer.get(AoA).size() && row + inner < rowCount && col - inner >= 0 && !sequenceFound; inner++) {
		                if (!twoArray[row + inner][col - inner].equals(outer.get(AoA).get(inner))) {
		                    break;
		                }
		            }
		            if (inner == outer.get(AoA).size()) {
		                for (int i = 0; i < inner; i++) {
		                   
		                    String numR =(row + i) + "";
		                    String numC = (col - i) + "";
		                    nums.add(numR);
		                    nums.add(numC);
		                }
		                sequenceFound = true;
		            }

		            // Check for reverse left diagonal match
		            for (inner = 0; inner < outer.get(AoA).size() && row + inner < rowCount && col - inner >= 0 && !sequenceFound; inner++) {
		                if (!twoArray[row + inner][col - inner].equals(outer.get(AoA).get(inner))) {
		                    break;
		                }
		            }
		            if (inner == outer.get(AoA).size()) {
		                for (int i = 0; i < inner; i++) {
		                    String numR = (row + i) + "";
		                    String numC = (col - i) + "";
		                    nums.add(numR);
		                    nums.add(numC);
		                }
		                sequenceFound = true;
		            }
		        }
		            
		            
		        }
		    }
		

		System.out.println();
		//System.out.println(outer);

		
		filePrint(nums,ou);
		
		
	
		
	}
	public static void filePrint(ArrayList<String> num,ArrayList<String>ou)
	{
		
		ArrayList<String>nums = num;
		ArrayList<String>List = ou;
		ArrayList<Integer> Size = new ArrayList<>();
		//System.out.println(nums);
		//System.out.println(List);		
		
		
		
			
			for(int x =0; x<List.size();x++)
			{
				Size.add(List.get(x).length()*2);
			}
			//System.out.println(Size);
			
			
			
			
			
			 int currentIndex = 0;
		        for (int x = 0; x < List.size(); x++) {
		            String s = List.get(x);
		            String start = nums.get(currentIndex) + ":" + nums.get(currentIndex + 1);
		            currentIndex += Size.get(x) - 2;
		            String end = nums.get(currentIndex) + ":" + nums.get(currentIndex + 1);
		            currentIndex += 2;

		            String print = s + " " + start + " " + end;

		            // Write the contents to the output file
		           System.out.println(print);
		            
		        }
			
		
		
		
	}
	
}
		
	
	

